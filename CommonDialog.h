#include <string>
#include <shlobj.h>

////////////////////////////////////////////////////////////////////////////////
// 
// ■ 各種コモンダイアログボックスを開いて情報取得するライブラリ
// ・必要に応じてこのファイルをカスタマイズします
// ・windowsでよく使われる汎用ダイアログボックスを呼び出すライブラリです
//   ファイルやフォルダを選択したり、フォントや色情報を取得する場合に使用します
// 
////////////////////////////////////////////////////////////////////////////////

static int CALLBACK SHBrowseProc(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData){
	if( uMsg == BFFM_INITIALIZED && lpData ) SendMessageA( hWnd, BFFM_SETSELECTION, TRUE, lpData); //  デフォルトで選択させるパスの指定
    return 0;
}
static void SHFree(ITEMIDLIST* pidl){
	IMalloc*  pMalloc; SHGetMalloc( &pMalloc );
	if ( pMalloc ){ pMalloc->Free( pidl ); pMalloc->Release();}
}

//----------------------------------------------------------
// 「フォルダを開く」ダイアログ表示し、フォルダ情報(フォルダ名folderName, 絶対パスfolderPath)を得る
//----------------------------------------------------------
BOOL OpenCommonDialogFolder( std::string& folderName, std::string& folderPath, const HWND hWnd, const char* caption){
	BROWSEINFOA  bi; ZeroMemory( &bi, sizeof( BROWSEINFO ));
	char    szSelectedPath[_MAX_PATH], szSelectedName[_MAX_PATH];
#ifdef UNICODE
	wchar_t szDefaultPath[_MAX_PATH]; mbstowcs_s(NULL, szDefaultPath, folderPath.data(), _MAX_PATH);
#else
	char    szDefaultPath[_MAX_PATH]; strcpy_s(szDefaultPath, _MAX_PATH,folderPath.data());
#endif

	bi.lpfn           = SHBrowseProc;	//  コールバック関数を指定
	bi.lParam         = (LPARAM)szDefaultPath;	//  デフォルトで選択させておくフォルダを指定
	bi.pszDisplayName = szSelectedName;
	bi.hwndOwner      = hWnd;
	bi.lpszTitle      = caption;	//  タイトルの指定
	ITEMIDLIST*  pidl = SHBrowseForFolderA( &bi );	//  フォルダダイアログの起動
	if( pidl ){
		//  選択されたフォルダ名を取得
		SHGetPathFromIDListA( pidl, szSelectedPath );
		SHFree(pidl);
		folderName =  szSelectedName;
		folderPath =  szSelectedPath;
		return TRUE;	//  フォルダが選択された
	}
	return FALSE;	//  フォルダは選択されなかった
}

//----------------------------------------------------------
// 「ファイルを開く」ダイアログ表示し、ファイル情報(OPENFILENAME構造体)を得る
//----------------------------------------------------------
BOOL OpenFileDialogA(OPENFILENAMEA& OFN, const HWND hWnd, const char* Title, const char* Filter){
	char	FilePath[_MAX_PATH*256];

	ZeroMemory(FilePath,MAX_PATH);
	ZeroMemory(&OFN,sizeof(OPENFILENAME));

	OFN.lStructSize	= sizeof(OPENFILENAME); 
	OFN.hwndOwner	= hWnd;
	OFN.lpstrFilter	= Filter;
	OFN.lpstrFile	= FilePath;
	OFN.nMaxFile	= sizeof(FilePath)/sizeof(TCHAR);
	OFN.Flags = OFN_EXPLORER | OFN_ENABLESIZING | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT;
	OFN.lpstrTitle	= Title;

	return GetOpenFileNameA(&OFN);
}

//----------------------------------------------------------
// 「フォント」ダイアログ表示し、フォント情報(LOGFONT構造体, カラー)を得る
//----------------------------------------------------------
BOOL OpenCommonDiaogFont(LOGFONT& logfont, COLORREF& fontcolor, const HWND hWnd){
	CHOOSEFONT CHF;

	// CHOOSEFONT構造体の設定
	memset(&CHF, 0, sizeof(CHOOSEFONT));
	CHF.lStructSize  = sizeof(CHOOSEFONT);
	CHF.hwndOwner  = hWnd;
	CHF.lpLogFont  = &logfont;   // フォント属性規定値
	CHF.Flags   = CF_SCREENFONTS | CF_EFFECTS | CF_INITTOLOGFONTSTRUCT;
	CHF.rgbColors  =  fontcolor;   // 色規定値
	CHF.nFontType  = SCREEN_FONTTYPE;

	// フォント(CommonDialogBox)オープン
	if(ChooseFont(&CHF)){
		fontcolor = CHF.rgbColors;
		return TRUE;
	}else{
		return FALSE;
	}
}

//----------------------------------------------------------
// 「色の編集」ダイアログ表示し、色情報(選択色selectColor, 作成した色配列customColors)を得る
//----------------------------------------------------------
BOOL OpenCommonDiaogColor(COLORREF& selectColor, COLORREF customColors[16], const HWND hWnd){
	if(customColors == NULL) return FALSE;
//	for(int i=0; i<16; i++) *customs[i] = RGB(255,255,255); // 「作成した色」を保存せず全て白にする

	CHOOSECOLOR cc;
	memset(&cc, 0, sizeof(CHOOSECOLOR));
	cc.lStructSize  = sizeof(CHOOSECOLOR);
	cc.hwndOwner    = hWnd;
	cc.rgbResult    = selectColor;      // 初期カラー
	cc.lpCustColors = customColors;     // 「作成した色」枠カラー
	cc.Flags        = CC_FULLOPEN | CC_RGBINIT;

	if(ChooseColor(&cc)){
		selectColor = cc.rgbResult;
		return TRUE;
	}else{
		return FALSE;
	}
}