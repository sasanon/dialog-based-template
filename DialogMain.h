#pragma once
#include <windows.h>
#include "resource.h"
#include "CommonDialog.h"

////////////////////////////////////////////////////////////////////////////////
// 
// ■ ダイアログボックスの挙動定義
// ・必要に応じてこのファイルをカスタマイズします
// ・ボタン押下等のメッセージを受け取るDlgProc関数(ウィンドウプロシージャ)と
//   その処理部分を外部に出してまとめたDialogEventHandlerクラスからなります
// ・Dialogクラスの処理が長くなる場合は別ファイルに関数を書くなど分割のこと
// 
////////////////////////////////////////////////////////////////////////////////

// ダイアログ イベントハンドラ(ボタン押下等に対応する処理)クラス
class DialogEventHandler{
	HWND hWnd;
public:
	// ダイアログボックスが作られた際の初期化処理
	void OnInit(HWND hSrcWnd){
		hWnd = hSrcWnd;	// ウィンドウハンドラを保存
	}

	// ※「ほげー」ボタンが押されたらIDC_BUTTON_HOGEを通してこの関数が実行されます
	void OnHogehoge(WORD wp){
		// OKメッセージボックスの使用例
		MessageBoxA(hWnd, "ほげー", "ほげーが押されました", MB_OK);
	}

	// ※「ふがー」ボタンが押されたらIDC_BUTTON_FUGAを通してこの関数が実行されます
	void OnFugafuga(WORD wp){
		// YES/NOメッセージボックスの使用例
		if(MessageBoxA(hWnd, "ふがー", "ふがーが押されました", MB_YESNO) == IDYES){
			MessageBoxA(hWnd, "いえす！", "いえすを押したよ", MB_OK);
		}else{
			MessageBoxA(hWnd, "のー。", "のーを押したよ", MB_OK);
		}
	}
};
static DialogEventHandler dialog;

//ウィンドウプロシージャ(モードレスダイアログ)
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp){
	// windowsメッセージ毎の処理
	switch (msg){
		case WM_INITDIALOG: dialog.OnInit(hWnd);                break;	//ウインドウ作成時の処理
		case WM_CLOSE:      PostMessage(hWnd,WM_DESTROY,0,0);   break;	//右上[X]ボタン
		case WM_DESTROY:    PostQuitMessage(0);                 break;	//プログラム終了時のウインドウ破棄
		case WM_COMMAND: //ユーザーメッセージ処理
			switch(LOWORD(wp)){
				case IDC_BUTTON_HOGE:	dialog.OnHogehoge(LOWORD(wp));	break;	// "ほげー"ボタン
				case IDC_BUTTON_FUGA:	dialog.OnFugafuga(LOWORD(wp));	break;	// "ふがー"ボタン
				default:	return FALSE;	// ダイアログベースではDefWindowProcしない
			}
			break;
		default: return FALSE; // ダイアログベースではDefWindowProcしない
	}
	return TRUE;	// 実装したメッセージはbreakしてこのreturnからwindows制御に戻る
}
